# requirements
git, npm

# setup
```
git clone git@gitlab.com:domezi/kpmj_dialogflow.git
cd kpmj_dialogflow
npm i
```

# run locally
`npm start`

# push to production
`git add . &&  git commit -m 'the commit message' && git push heroku main`
