// KPMJ - WEBHOOK

// HTTP CONFIG
const { initializeApp } = require("firebase/app")
const { getFirestore, collection, doc, addDoc, setDoc, getDoc, getDocs } = require('firebase/firestore/lite')
const { Timestamp } = require("firebase/firestore");
const http = require("http");
const port = process.env.PORT || 3000;
const hostname = port != 3000 ? "0.0.0.0" : "127.0.0.1";
const express = require('express')
const app = express()

// EXPRESS SETUP
var bodyParser = require('body-parser')
app.use( bodyParser.json() );      
app.use(bodyParser.urlencoded({    
  extended: true
}));
app.use(express.json());

// DIALOGFLOW OUTPUT FUNC
function replyCX(res,msg,options=[],isSpecificEmotion=false) {

    let json = {
        "fulfillmentResponse":{
            "messages":[
                ...msg.split('\n').map(line => {
                    return   { "text":{ "text": [line] } }
                }),
                { "payload": {
                        "isSpecificEmotion":isSpecificEmotion,
                        "richContent": [ {
                            "options":options.map(o=>{return {text:o}}),
                            "type": "chips"
                        } ]
                    } }
            ]
        }
    }

    console.log("Sending",json)

    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(json))
}


// HTTP CX
app.all('/cx', (req, res) => {

    // FIREBASE CONFIG
    const firebaseConfig = {
        apiKey: "AIzaSyDui4EgzTR2RzF5XpQFam1i4_nndrh6-Os",
        authDomain: "kpmj-6cbdf.firebaseapp.com",
        projectId: "kpmj-6cbdf",
        storageBucket: "kpmj-6cbdf.appspot.com",
        messagingSenderId: "613691456238",
        appId: "1:613691456238:web:3142f2b4b28f51501d91ff"
    };

    // SETUP
    initializeApp(firebaseConfig);
    const db = getFirestore();
    const dt = new Date()
    //const intent=req.body.queryResult.intent.displayName
//---

    console.log("CX BODY");
    console.log(req.body)
// CX intent geetting
    const intentInfo=req.body.intentInfo
    let intent = null
    let slot= false

    if(intentInfo == undefined) {
        const page_intent=req.body.pageInfo.displayName
        if( page_intent == 'MoodTrackingPage')
            intent = 'WelcomeMoodTracking'
        else if( page_intent == 'CalendarScorePage')
            intent = 'calendar.score'
    } else intent=intentInfo.displayName
    if(req.body.fulfillmentInfo != undefined) {
        slot = req.body.fulfillmentInfo.tag
    }

//const intent=req.body.intentInfo.displayName

//---
    // MOOD TRACKING INTENT
    if(intent === 'WelcomeMoodTracking') {

        // DEFS
        let specificEmotions = {
            "angry":"enraged,offended,irritable,aggressive,frustrated",
            "afraid":"terrified,scared,worried,helpless,connected,nervous",
            "sad":"hurt,heartborken,shameful,disappointed,gloomy,droppy,fragile",
            "lonely":"depressing,unwanted,excluded,isolated,missing",
            "connected":"intimate,loved,cooperative,attracted,enthusiastic,passionate",
            "happy":"passionate,proud,happy,joyful,pleased,amazed,cheerful",
            "excited":"aroused,energetic,optimistic,moved",
            "surprised":"confused,stunned,alarmed,overcome,nervous",
        }

        var specificEmotion, baseEmotion, hasActivities, activities;

        if(req.body.sessionInfo.parameters != undefined) {
            baseEmotion = req.body.sessionInfo.parameters.baseemotion
            specificEmotion = req.body.sessionInfo.parameters.specificemotion
            activities = req.body.sessionInfo.parameters.activities
        }

        // SLOT FILLING
        if(slot == "baseEmotion") {

            replyCX(res,t('ask_baseEmotion'),Object.keys(specificEmotions));

        } else if(slot == "env_prod") {

            replyCX(res,("Hallooooo ich bin 1 ENV_PROD RESPONSE"));

        } else if(slot == "specificEmotion") {

            options = specificEmotions[baseEmotion.toLowerCase()]
            if(options == undefined) options = "e.g. proud";

            replyCX(res,t('ask_specificEmotion'),options.split(","),true);

        } else if(slot == "activities") {

            replyCX(res,t('ask_activities'));

            //} else if(slot == "hasActivity") {

            //replyCX(res,"thank you, this is 'has actiivity' slot");

        } else {

            console.log("FINALLY, weve got all parameters together. Now writing to database")
            // SLOT FILLING COMPLETED

            // PREPARE RESPONSE
            let myDoc = {
                activities: activities == "no" || activities == "No" ? [] : activities.split(","),
                emotion: specificEmotion,
                createdAt: new Date(),
            }

            //return;//for actual test

            // WRITE TO DATABASE
            //const user_id = req.body.originalDetectIntentRequest.payload.userId //TODO anpassen für CX
            const user_id = "INSERT_USER_ID"
            setDoc(doc(collection(db, "user/"+user_id+"/journal/"), generateId()), myDoc )
                .then((docRes)=>{

                    // RETURN TO USER
                    replyCX(res,
                        t("res_you_feel",{specificEmotion})
                        +"\n"+
                        (activities == "no" ?
                            t("i_wont_attach_activities")
                            :t("i_will_attach_activities",{activities}))
                    );
                    console.log("Written to DB successfully :D");

                }).catch(e=>{

                // ERROR
                replyCX(res,t("wh_error"))
                console.log("Could write to DB");

            })

        }


    } else if(intent === 'calendar.score') {

        // cofiguration
        const month = dt.getFullYear()+"-"+(dt.getMonth() + 1).toString().padStart(2, "0")
        const today = {day: dt.getDate(), score: req.body.queryResult.parameters.score}
        const user_id = req.body.originalDetectIntentRequest.payload.userId

        // perform crud operation
        // /user/dbeumjsRRkZGYLjgXmTkpBudEa92/journal/M0sTTII68m1VZfoUWY0n
        const docRef = doc(db, "users/"+user_id+"/journal/",month);
        getDoc(docRef).then((docRes)=>{

            let myDoc =  undefined // docRes.data()
            if(myDoc == undefined) {
                console.log("creating month")
                myDoc = {scores:{}}
            }
            myDoc.scores[today.day] = today.score

            console.log("kurz beovr, collection set",myDoc)
            setDoc(doc(collection(db, "users/"+user_id+"/journal/"), month), myDoc );
            console.log("success, collection set",myDoc)

            replyCX(res,"Okay, I have noted down a score of "+today.score+" for todays date, the "+today.day+"th.")
        }).catch(e=>{
            console.log("catched e",e)
            replyCX(res,"wh error")
        })
    } else {

        replyCX(res,"Webhook says I can't see any functions for the intent "+intent+". I apologize")

    }


});


// TRANSLATION FUNC
function t(slug, vars={}) {
    let strings = {
        'ask_baseEmotion':`I am glad to hear that you have decided to track your mood. 😃
		First please choose an emotion that most closely reflects how you feel at the moment.  `,
		
		//ANSWER BASE EMOTIONS
		'answer_baseEmotion_happy': `Yeah happy. 😇 
				That sounds great!`,
		'answer_baseEmotion_excited': `Wow there are diferent ways to feeling excited. 🤔`,
		'answer_baseEmotion_surpriesd': `Okay sounds like something happened that you weren't expecting. 🤔`,
		'answer_baseEmotion_connected': `Great, connections are so important. `,
		'answer_baseEmotion_sad': `Ohh ☹️ Let's go a little deeper to find out why you feel sad.`,
		'answer_baseEmotion_lonely': `Oh sorry. 😔 
				Maybe we can find out why you feel that way. `,
		'answer_baseEmotion_afraid': `I am sorry that you feel fear. 
				Lets analyze why you feel this way.`,
		'answer_baseEmotion_angry': `Ohh you feel angry. 
				I hope we can figure out why. 😫`,
		
        'ask_specificEmotion':'Please pick one of this more specific emotions which you think describes it in the best way.',
		
		//ANSWER SPECIFIC EMOTIONS
		//happy
		'answer_specificEmotion_happy': [
			`${vars.specificEmotion}, nice i can feel you! 😁`,
			
			`Awesome! 😁 
				${vars.specificEmotion} is a great feeling! `
		],
		//excited
		'answer_specificEmotion_excited': [
		
			`Yeah ${vars.specificEmotion} 😁`,
			
			`Great you feel ${vars.specificEmotion}! 😁 
				is a great feeling! `
		],
		//surprised
		'answer_specificEmotion_surpriesd': [
		
			`Okay ${vars.specificEmotion}.`//TODO surprised emotions could be positive or negative
		],
		//connected
		'answer_specificEmotion_connected': [
		
			`${vars.specificEmotion}, great. 
				People around us are so important`,
				
			`Awesome! 😁 
				${vars.specificEmotion} is a great feeling! `
		],
		//sad
		'answer_specificEmotion_sad': [
		
			`${vars.specificEmotion}... i understand.`,
			
			`I am sure that there will come better days!`
		],
		//lonely
		'answer_specificEmotion_lonely': [
		
			`${vars.specificEmotion} - i know how this feels like.`,
			
			`Okay i understand.
				You can do something to feel more connected.`
		],
		//afraid
		'answer_specificEmotion_afraid': [
		
			`Okay i understand.`,
			
			`${vars.specificEmotion} i noticed! 
				But with your courage you are able to conquer your fear.`
		],
		//angry
		'answer_specificEmotion_angry': [
		
			`Okay i understand.`,
			
			`${vars.specificEmotion} i noticed! 
				Take a deep breath for a moment and then move on. `
		],
		
		/*
		//Happy
		'answer_specificEmotion_happy': ``,
		'answer_specificEmotion_enthusiastic': ``,
		'answer_specificEmotion_cheerful': ``,
		'answer_specificEmotion_proud': ``,
		'answer_specificEmotion_optimistic': ``,
		'answer_specificEmotion_joyful': ``,
		'answer_specificEmotion_relaxed': ``,
		'answer_specificEmotion_optimistic': ``,
		//Excited
		'answer_specificEmotion_amazed': ``,
		'answer_specificEmotion_aroused': ``,
		'answer_specificEmotion_energetic': ``,
		'answer_specificEmotion_moved': ``,
		'answer_specificEmotion_surprised': ``,
		'answer_specificEmotion_optimistic': ``,
		
		//Surprised
		'answer_specificEmotion_confuaed': ``,
		'answer_specificEmotion_stunned': ``,
		'answer_specificEmotion_amazed': ``,
		'answer_specificEmotion_shocked': ``,
		'answer_specificEmotion_alarmed': ``,
		'answer_specificEmotion_overcome': ``,
		'answer_specificEmotion_moved': ``,
		//Connected
		'answer_specificEmotion_loved': ``,
		'answer_specificEmotion_attracted': ``,
		'answer_specificEmotion_committed': ``,
		'answer_specificEmotion_cooperative': ``,
		'answer_specificEmotion_intimate': ``,
		'answer_specificEmotion_interconnected': ``,
		//Sad
		'answer_specificEmotion_depressed': ``,
		'answer_specificEmotion_exhausted': ``,
		'answer_specificEmotion_melancholic': ``,
		'answer_specificEmotion_shameful': ``,
		'answer_specificEmotion_hopeless': ``,
		'answer_specificEmotion_discouraged': ``,
		'answer_specificEmotion_hurt': ``,
		'answer_specificEmotion_grieving': ``,
		'answer_specificEmotion_heartbroken': ``,
		//Lonely
		'answer_specificEmotion_loved': ``,
		'answer_specificEmotion_attracted': ``,
		'answer_specificEmotion_committed': ``,
		'answer_specificEmotion_cooperative': ``,
		'answer_specificEmotion_intimate': ``,
		'answer_specificEmotion_interconnected': ``,
		//Afraid
		'answer_specificEmotion_stressed': ``,
		'answer_specificEmotion_worried': ``,
		'answer_specificEmotion_scared': ``,
		'answer_specificEmotion_terrified': ``,
		'answer_specificEmotion_insecure': ``,
		'answer_specificEmotion_nervous': ``,
		'answer_specificEmotion_guilty': ``,
		'answer_specificEmotion_hopeless': ``,
		//Angry
		'answer_specificEmotion_frustrated': ``,
		'answer_specificEmotion_aggresive': ``,
		'answer_specificEmotion_offended': ``,
		'answer_specificEmotion_enraged': ``,
		'answer_specificEmotion_irritable': ``,
		*/
		
        'ask_activities':`Okay i understand.
			Please think about why you feel this way? Are there specific reasons why you feel happy? Maybe special activities, people or other things.
			Type them seperated by comma or if you don't want to, type 'no'.`,

        'res_you_feel':`Thank you. You feel ${vars.specificEmotion}.`,

        'i_wont_attach_activities':`I won t attach activities`,
        'i_will_attach_activities':`I will insert the following activities: ${vars.activities}.\nIf you want to add another emotion, please type 'start mood tracking' again.`,

        'wh_error':'Webhook error.',
        'lang_error':'No translation found' 
    }
    return strings[slug] == undefined ? strings['lang_error'] : strings[slug];
}

// UUID GEN FUNC
function generateId() {
    var sym = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
    var str = '';
    for(var i = 0; i < 20; i++) {
        str += sym[parseInt(Math.random() * (sym.length))]; 
    }
    return str
}

// DIALOGFLOW OUTPUT FUNC
function reply(res,msg,options=[],isSpecificEmotion=false) {

    let json = {
        "fulfillmentMessages": [
            {
                "text": {
                    "text": msg.split('\n')
                }
            },
            {
                "payload": {
                    "isSpecificEmotion":isSpecificEmotion,
                    "richContent": [
                        [
                            {
                                "options":options.map(o=>{return {text:o}}),
                                "type": "chips"
                            }
                        ]
                    ]
                }
            }
        ]
    }

    console.log(msg)

    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify(json))
}


// HTTP
// ES
app.all('/', (req, res) => {

        // FIREBASE CONFIG
        const firebaseConfig = { 
          apiKey: "AIzaSyDui4EgzTR2RzF5XpQFam1i4_nndrh6-Os",
          authDomain: "kpmj-6cbdf.firebaseapp.com",
          projectId: "kpmj-6cbdf",
          storageBucket: "kpmj-6cbdf.appspot.com",
          messagingSenderId: "613691456238",
          appId: "1:613691456238:web:3142f2b4b28f51501d91ff"
        };

        // SETUP
        initializeApp(firebaseConfig);
        const db = getFirestore();
        const dt = new Date()
        const intent=req.body.queryResult.intent.displayName

        // MOOD TRACKING INTENT
        if(intent === 'WelcomeMoodTracking') {

            // DEFS
            let specificEmotions = {
                "angry":"enraged,offended,irritable,aggressive,frustrated",
                "afraid":"terrified,scared,worried,helpless,connected,nervous",
                "sad":"hurt,heartborken,shameful,disappointed,gloomy,droppy,fragile",
                "lonely":"depressing,unwanted,excluded,isolated,missing",
                "connected":"intimate,loved,cooperative,attracted,enthusiastic,passionate",
                "happy":"passionate,proud,happy,joyful,pleased,amazed,cheerful",
                "excited":"aroused,energetic,optimistic,moved",
                "surprised":"confused,stunned,alarmed,overcome,nervous",
            }

            // DECONSTRUCT REQUEST
            const { specificEmotion, baseEmotion, hasActivities, activities } = req.body.queryResult.parameters

            // SLOT FILLING
            if(baseEmotion == "") {

                reply(res,t('ask_baseEmotion'),Object.keys(specificEmotions));

            } else if(specificEmotion == "") {

                options = specificEmotions[baseEmotion.toLowerCase()]
                if(options == undefined) options = "e.g. proud";

                reply(res,t('ask_specificEmotion'),options.split(","),true);

            } else if(activities == "") {

                reply(res,t('ask_activities'));

            } else {
                
                // SLOT FILLING COMPLETED

                // PREPARE RESPONSE
                let myDoc = {
                    activities: activities == "No" || activities == "no" ? [] : activities.split(","),
                    emotion: specificEmotion,
                    createdAt: new Date(),
                }


                // WRITE TO DATABASE
                const user_id = req.body.originalDetectIntentRequest.payload.userId
                setDoc(doc(collection(db, "user/"+user_id+"/journal/"), generateId()), myDoc )
                    .then((docRes)=>{

                        // RETURN TO USER
                        reply(res,
                            t("res_you_feel",{specificEmotion})
                            +"\n"+
                            (activities == "No" || activities == "no" ?
                            t("i_wont_attach_activities")
                            :t("i_will_attach_activities",{activities}))
                        );

                    }).catch(e=>{

                        // ERROR
                        reply(res,t("wh_error"))

                    })
                   
            }


        } else if(intent === 'calendar.score') {

            // cofiguration
            const month = dt.getFullYear()+"-"+(dt.getMonth() + 1).toString().padStart(2, "0")
            const today = {day: dt.getDate(), score: req.body.queryResult.parameters.score}
            const user_id = req.body.originalDetectIntentRequest.payload.userId
            
            // perform crud operation
            // /user/dbeumjsRRkZGYLjgXmTkpBudEa92/journal/M0sTTII68m1VZfoUWY0n
            const docRef = doc(db, "users/"+user_id+"/journal/",month);
            getDoc(docRef).then((docRes)=>{

                let myDoc =  undefined // docRes.data()
                if(myDoc == undefined) {
                    console.log("creating month")
                    myDoc = {scores:{}}
                }
                myDoc.scores[today.day] = today.score

                console.log("kurz beovr, collection set",myDoc)
                setDoc(doc(collection(db, "users/"+user_id+"/journal/"), month), myDoc );
                console.log("success, collection set",myDoc)

                reply(res,"Okay, I have noted down a score of "+today.score+" for todays date, the "+today.day+"th.")
            }).catch(e=>{
                console.log("catched e",e)
                reply(res,"wh error")
            })
        } else {

            reply(res,"I can't see any functions for the intent "+intent+". I apologize")

        }


});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

